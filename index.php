<html>

	<head>
		<title>Banco topper</title>
		<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
	</head>
	
	<body>
	
		<nav class="fixed bg-gray-800 h-24 w-screen">
            <div class="flex flex-row justify-center items-center h-full w-full">
                <p class="text-3xl text-white font-bold">BANCO TOPPER</p>
            </div>
		</nav>
		
		<main class="h-screen w-screen">
			<div class="flex flex-col justify-center w-2/3 h-full mx-auto text-center">
                <p class="text-3xl text-gray-900 font-bold">Entrar</p>
                <div class="flex flex-row justify-center w-full my-2">
                    <div class="w-full max-w-xs">
                        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 border-gray-400 border">
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                                    Email
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" type="text" placeholder="Email">
                            </div>
                            <div class="mb-6">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                                    Senha
                                </label>
                                <input class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="******************">
                                <p class="text-red-500 text-xs italic">Por favor, insira uma senha.</p>
                            </div>
                            <div class="flex items-center justify-between">
                                <button class="text-gray-700 hover:text-gray-800 font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                                    Cadastrar
                                </button>

                                <button class="bg-gray-700 hover:bg-gray-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                                    Entrar
                                </button>
                            </div>
                        </form>
                        <p class="text-center text-gray-500 text-xs">
                            &copy;2019 Banco topper. Todos os direitos reservados.
                        </p>
                    </div>
                </div>
			</div>
        </main>
		
	</body>
	
</html>